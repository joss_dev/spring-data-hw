package com.bsa.springdata.project;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface ProjectRepository extends JpaRepository<Project, UUID> {

    @Query("select p from Project p where p in " +
            "(select t.project from Team t where t.technology.name = :technology)")
    List<Project> findTop5ByTechnology(String technology, Pageable pageable);


    //Better use native query
    @Query("select count(distinct p) from Team t " +
            "inner join t.project p " +
            "inner join t.users u " +
            "inner join u.roles r where r.name= :role")
    int getCountWithRole(String role);
}