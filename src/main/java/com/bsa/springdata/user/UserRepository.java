package com.bsa.springdata.user;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface UserRepository extends JpaRepository<User, UUID> {

    List<User> findByLastNameStartsWithIgnoreCase(String lastName, Pageable pageable);

    @Query("select u from User u where u.office.city = :city")
    List<User> findAllByCity(String city, Sort sort);

    List<User> findByExperienceGreaterThanEqual(int experience, Sort sort);

    @Query("select u from User u where u.office.city = :city and u.team.room = :room")
    List<User> findAllByCityAndRoom(String city, String room, Sort sort);

    //    @Modifying
//    @Query("delete from User u where u.experience < :experience")
    int deleteByExperienceLessThan(int experience);
}
