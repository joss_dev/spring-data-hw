package com.bsa.springdata.team;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface TeamRepository extends JpaRepository<Team, UUID> {

    @Modifying
    @Query(value = "update teams t set name = concat_ws('_', name, " +
            "(select name from projects where id = t.project_id), " +
            "(select name from technologies where id = t.technology_id)) " +
            "where name = :hipsters", nativeQuery = true)
    void normalizeName(String hipsters);

    Optional<Boolean> findByName(String teamName);

    int countByTechnologyName(String newTechnology);

}
