package com.bsa.springdata.team;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class TeamService {

    @Autowired
    private TeamRepository teamRepository;
    @Autowired
    private TechnologyRepository technologyRepository;

    @Transactional
    public void updateTechnology(int devsNumber, String oldTechnologyName, String newTechnologyName) {
        var teamsCount = teamRepository.countByTechnologyName(oldTechnologyName);

        if (teamsCount < devsNumber)
            technologyRepository.updateTechnology(oldTechnologyName, newTechnologyName);
    }

    @Transactional
    public void normalizeName(String hipsters) {
        teamRepository.normalizeName(hipsters);
    }
}
