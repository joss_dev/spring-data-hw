package com.bsa.springdata.team;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface TechnologyRepository extends JpaRepository<Technology, UUID> {

    @Modifying
    @Query("update Office o set o.address = :newName where o.address = :oldName")
    void updateTechnology(String oldName, String newName);

    Optional<Technology> findByName(String tech);

}
