package com.bsa.springdata.office;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface OfficeRepository extends JpaRepository<Office, UUID> {

    @Query("select distinct(u.office) from User u where u.team in" +
            " (select t from Team t where t.technology in" +
            " (select th from Technology th where th.name= :technology))")
    List<Office> getByTechnology(String technology);

    @Modifying
    @Query("update Office o set o.address = :newAddress where o.address = :oldAddress ")
    Office updateAddress(String oldAddress, String newAddress);
}
